---
layout: markdown_page
title: "Category Vision - Release Orchestration"
---

- TOC
{:toc}

## Release Orchestration

Release Orchestration is the ability to coordinate complex releases, particularly
across projects, in an efficient way that leverages as-code behaviors as much as
possible, but also recognizes that there are manual steps and coordination points
involving human decisions throughout software delivery in the enterprise. More
specifically, this is managing the kinds of complex enterprise releases for which you'd
have a Release Manager in play, rather than having individual teams continually
deploying independent code to production. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Orchestration)
- [Overall Vision](/direction/cicd#release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/releases/index.html)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1298) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## Manual Processes vs. Automation

One important consideration for how we want to implement release orchestration at
GitLab is that we want to solve automated, continuous delivery workflows first.
We believe that all enterprises can achieve this, and that GitLab as a whole
is a powerful tool to unlock efficiency within DevOps. As such we avoid adding
manual, non-automated controls to the delivery pipeline. That's not to say we don't
want to hear about problems that are currently being solved with manual process,
or that we just don't want to address them, but we want to work with you to creatively
find ways to solve these with automation. Only if that becomes untenable do we
want to look at other options. Ultimately we believe this is best
 for GitLab and for our users; we'd much rather see  people doing release
management using the data that already exists, by using our tool for day to day actions 
and processes and not having to do any manual work on top of it.

### Compliance and Security in the Release Pipeline

Security, compliance, control and governance of the release pipeline is handled
as part of [Release Governance](/direction/release/release_governance). Secrets
in the pipeline are part of our [Secrets Management](/direction/release/secrets_management) category.

## What's Next & Why

Up next we'll be adding an environments dashboard ([gitlab#3713](https://gitlab.com/gitlab-org/gitlab/issues/3713)),
giving complete visibility over what's deployed to the environments for your project.
For release managers, environments are often the context you're working in - what
releases are deployed to which environments and what is their status? This view will
help provide visibility in that context.

Users who are using external environments and deployments (for example: Heroku) will now be able to see when a merge request was deployed to production ([gitlab#22513](https://gitlab.com/gitlab-org/gitlab/issues/22513)).
 
In addition, we are investing a lot of effort to make the releases page the anchor of release orchestration by adding the ability to edit the release from the UI ([gitlab#26016](https://gitlab.com/gitlab-org/gitlab/issues/26016)) and allowing milestone(s) to be associated with a release via
([gitlab#29020](https://gitlab.com/gitlab-org/gitlab/issues/29020)).

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
We believe that adding capabilities to monitor and edit the release directly from the release page will progress the maturity to Viable as it will connect the issues that the development team is working on for a specific milestone and/or release automatically into the workflow.


Key deliverables to achieve this are:

- [Create draft releases](https://gitlab.com/gitlab-org/gitlab-foss/issues/38105) (Complete)
- [Cross-project environment dashboard](https://gitlab.com/gitlab-org/gitlab/issues/3713)
- [Edit Release](https://gitlab.com/gitlab-org/gitlab/issues/26016)
- [Association of milestones with releases](https://gitlab.com/gitlab-org/gitlab/issues/29020)
- [Filter issues based on release](https://gitlab.com/gitlab-org/gitlab/issues/32632)
- [Capture Release actions in the audit log page](https://gitlab.com/gitlab-org/gitlab/issues/32807)
- [Release generation from within `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/issues/26013)
- [Auto chagelog for release](https://gitlab.com/gitlab-org/gitlab/issues/26015)
 


## Competitive Landscape

Release orchestration tools tend to have great features for managing releases,
as you'd expect; they are built from the ground up as a release management
workflow tool and are very capable in those areas. Our approach to release
orchestration will be a bit different, instead of being workflow-oriented we
are going to approach release orchestration from a publishing point of view.
What this means is instead of building complicated workflows for your releases,
we will focus more on the artifact of the release itself and embedding the
checks and steps into it.

An important view for the way we look at the world is [gitlab#3713](https://gitlab.com/gitlab-org/gitlab/issues/3713)
which introduces an environments-based view for managing what's deployed.

 We are conducting a detailed comparrison to [XL Release](https://xebialabs.com/products/application-release-orchestration-xl-release/) and [Electric Cloud](https://electric-cloud.com/).

## Analyst Landscape

Application release orchestration (ARO) tools combine deployment automation, and pipeline and environment management with release orchestration capabilities to simultaneously improve the quality, velocity and governance of application releases.

Analysts at this time are looking for more quality of life features that make
a difference in people's daily workflows - how does this make my day better?
By introducing features like [gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015)
to automatically manage release notes as part of releases, we can demonstrate
how our solution is already capable of doing this.

## Top Customer Success/Sales Issue(s)

In terms of sales, a release orchestration dashboard that provides an environment dashboard that shows a view of what's deployed where:  [gitlab-org#3713](hhttps://gitlab.com/gitlab-org/gitlab/issues/3713).
This will provide our sales teams with a single, clear view that can easily
tell the story about how we solve their release orchestration problems.

## Top Customer Issue(s)

This is prospective given the feature is new, but customers typically look
for a bit more polish in features like this than the current MVC version
provides. Implementing [gitlab#26014](https://gitlab.com/gitlab-org/gitlab/issues/26014),
which makes creation of the release package an inline part of the
`.gitlab-ci.yml`, will make this feature feel much more mature and
production-ready (even if it is already really usable.)

## Top Internal Customer Issue(s)

A lot of interest has been expressed in adding the ability to delete environments ([gitlab#20620](https://gitlab.com/gitlab-org/gitlab/issues/20620) and [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724)).


## Top Vision Item(s)

An exciting item for the vision is the ability to create releases as
runbooks via [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427).
This will allow non-technical users to create runnable release plans in GitLab,
which can have actions embedded in them which will perform automated parts
of the release.
